package com.lzm.toolfx;

import com.lzm.toolfx.common.Property;
import com.lzm.toolfx.view.ConsoleView;
import com.lzm.toolfx.view.Splash;
import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ToolFxApplication extends AbstractJavaFxApplicationSupport {
    public static void main(String[] args) {
        new Property().execute();
        launch(ToolFxApplication.class, ConsoleView.class,new Splash(),args);
    }
}
