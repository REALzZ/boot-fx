package com.lzm.toolfx.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {
    private static final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String now(){
        return date.format(new Date()) +" -> ";
    }

    public static String now(SimpleDateFormat format){
        return format.format(new Date());
    }
}
