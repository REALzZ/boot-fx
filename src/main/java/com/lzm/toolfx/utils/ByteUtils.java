package com.lzm.toolfx.utils;

public class ByteUtils {
    /** 16进制转int整形 */
    public static int Hex2int(byte[] hexByteIn){
        int restult=0;
        for(int i=0;i<hexByteIn.length;i++) {
            int count = (hexByteIn.length-1-i)*8;
            restult += (hexByteIn[i] & 0x000000FF) << count;
        }
        return restult;
    }
}
