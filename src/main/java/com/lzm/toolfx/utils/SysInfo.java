package com.lzm.toolfx.utils;

import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.NetworkIF;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public final class SysInfo {
    static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private static oshi.SystemInfo systemInfo = new oshi.SystemInfo();
    private static HardwareAbstractionLayer hardwareAbstractionLayer = systemInfo.getHardware();


    public static List<Object> getIpList(){
        List<Object> ips = new ArrayList<>();
        ips.add("127.0.0.1");
        List<NetworkIF> networkIFS = hardwareAbstractionLayer.getNetworkIFs();
        for (NetworkIF networkIF : networkIFS) {
            for(String ip : networkIF.getIPv4addr()){
                if(!ip.isEmpty()){
                    ips.add(ip);
                }
            }
        }
        return  ips;
    }

    public static HardwareAbstractionLayer getSystemInfo(){
        return hardwareAbstractionLayer;
    }
    public static int getScreenWidth(){
        return (int)screenSize.getWidth();
    }
    public static int getScreenHeight(){
        return (int)screenSize.getHeight();
    }
}
