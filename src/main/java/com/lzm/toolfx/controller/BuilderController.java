package com.lzm.toolfx.controller;

import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.URL;
import java.util.ResourceBundle;

@FXMLController
public class BuilderController implements Initializable {

    @FXML
    AnchorPane ap_drag;
    @FXML
    Label dropped;
    @FXML
    ImageView iv_pic;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ap_drag.setOnDragOver(event -> {
            if (event.getGestureSource() != ap_drag
                    && event.getDragboard().hasFiles()) {
                /* allow for both copying and moving, whatever user chooses */
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            event.consume();
        });

        ap_drag.setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasFiles()) {
                dropped.setText(db.getFiles().get(0).getPath());
                success = true;
                String filePath = db.getFiles().get(0).getPath();
                System.out.println(filePath);
                Image img = new Image("file:/" + filePath);
                iv_pic.setFitWidth(img.getWidth());
                iv_pic.setFitHeight(img.getHeight());
                iv_pic.setImage(img);
            }
            /* let the source know whether the string was successfully
             * transferred and used */
            event.setDropCompleted(success);

            event.consume();
        });
    }
}
