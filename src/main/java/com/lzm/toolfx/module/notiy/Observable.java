package com.lzm.toolfx.module.notiy;


public interface Observable {
    public void registerObserver(SocketObserver o);
    public void removeObserver(SocketObserver o);
    public void notifyObserver();
}
