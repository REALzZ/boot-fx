package com.lzm.toolfx.module.notiy;

public interface SocketObserver {
    void update(NotifyType type, Object object);
}
