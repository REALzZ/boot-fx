package com.lzm.toolfx.module.notiy;

import java.util.HashSet;
import java.util.Set;

public class NotifyServer implements Observable {

    private Set<SocketObserver> list;
    private Object object;
    private NotifyType type;

    public NotifyServer() {
        list = new HashSet<>();
    }

    @Override
    public void registerObserver(SocketObserver o) {
        list.add(o);
    }

    @Override
    public void removeObserver(SocketObserver o) {
        if(!list.isEmpty())
            list.remove(o);
    }

    //遍历
    @Override
    public void notifyObserver() {
        for(SocketObserver observer:list) {
            observer.update(type,object);
        }
    }

    public void setNotify(NotifyType type, Object object) {
        this.object = object;
        this.type = type;
        notifyObserver();
    }

}
